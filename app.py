import codecs
import csv
import statistics
from datetime import datetime
from bokeh.embed import components
from bokeh.plotting import figure
from flask import Flask, request, render_template, redirect
import re
from lxml.doctestcompare import strip

import db


app = Flask(__name__, static_folder="static")


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/form')
def form():
    return render_template('upload.html')


@app.route('/upload', methods=['POST'])
def upload():
    uploaded_file = request.files['file']
    uploaded_filename = uploaded_file.filename

    if uploaded_filename != '':
        stream = codecs.getreader('utf-8')(uploaded_file.stream)
        header = stream.readline()

        m = re.search(r'Uploaded: ([0-9-]+ [0-9:]+) ', header)
        assert m, header[0]
        date = m.group(1)

        # get location and logger from filename
        # can't get from the file because some files have missing values in location
        m = re.match("^(.*)(P[0-9_ -]+)\.pm2$", uploaded_filename)
        assert m, uploaded_filename

        location = m.group(1).strip(' _')
        location = location.replace('#', '')
        location = location.replace(' ', '-')
        logger_id = m.group(2)

        # Skip 3 lines
        for _ in range(3):
            stream.readline()

        reader = csv.reader(stream, 'excel-tab')
        #data = pd.read_csv(uploaded_file,
        #                   skiprows=4,
        #                   sep='\t',
        #                   engine='python',
        #                   names=['timedate', 'temperature', 'relative_humidity'])

        db.insert_data(logger_id, location, date, reader)

        return redirect('/view', 303)


@app.route('/view')
def view():
    measurements = db.get_all_measurements()
    return render_template('explore.html', measurements=measurements)


@app.route('/report')
def report():
    startDate = request.args.get('startDate')
    endDate = request.args.get('endDate')
    timescale = request.args.get('timescale')
    room = request.args.get('room')
    stats = db.compute_statistics(startDate, endDate, room)

    # make lists for plots
    timedates = []
    temp = []
    rh = []

    for row in db.get_measurements(startDate, endDate, room):
        timedates.append((datetime.strptime(strip(row['timedate']), '%Y-%m-%d %H:%M')))
        temp.append(row['temperature'])
        rh.append(row['relative_humidity'])

    temp_plot = figure(title='Temperatures', x_axis_label='Dates', y_axis_label='Temperature', x_axis_type='datetime')
    rh_plot = figure(title='Relative Humidity', x_axis_label='Dates', y_axis_label='Relative Humidity', x_axis_type='datetime')

    temp_plot.line(timedates, temp, line_width=2)
    rh_plot.line(timedates, rh, line_width=2)

    script, div = components(temp_plot)
    script2, div2 = components(rh_plot)

    sorted_rh = sorted(rh)
    sorted_temp = sorted(temp)
    rh_median = statistics.median(sorted_rh)
    temp_median = statistics.median(sorted_temp)

    return render_template('report.html', stats=stats, rh_median=rh_median, temp_median=temp_median, startDate=startDate, endDate=endDate, timescale=timescale, room=room, rooms=db.get_rooms(), script = script, div = div, script2 = script2, div2 = div2)

if __name__ == '__main__':
    app.run()
