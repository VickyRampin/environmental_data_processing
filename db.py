import math
import sqlite3


def get_db():
    connection = sqlite3.connect('database.db', detect_types=sqlite3.PARSE_DECLTYPES)
    connection.row_factory = sqlite3.Row
    connection.execute("PRAGMA foreign_keys=ON;")
    return connection


def insert_data(logger_id, location, date, data):
    conn = get_db()

    cur = conn.execute('INSERT INTO uploads (logger_id, location, date_uploaded) VALUES (?, ?, ?)',
                       (logger_id, location, date))
    upload_id = cur.lastrowid

    SIZE = 100
    while True:
        params = []
        nrows = 0
        for _ in range(SIZE):
            try:
                row = next(data)
            except StopIteration:
                break
            nrows += 1
            params.extend((logger_id, location, upload_id, row[0], float(row[1]), float(row[2])))

        if nrows == 0:
            break

        conn.execute(
            """
            INSERT OR REPLACE INTO measurements(
                logger_id, location, upload_id, timedate, temperature, relative_humidity
            )
            VALUES %s;
            """ % ', '.join(['(?, ?, ?, ?, ?, ?)'] * nrows),
            params,
        )

    conn.commit()


def get_all_measurements():
    conn = get_db()
    cur = conn.cursor()
    measurements = cur.execute('SELECT timedate, logger_id, location, temperature, relative_humidity FROM measurements LIMIT 500')
    return measurements


def compute_statistics(startDate, endDate, room):
    conn = get_db()
    cur = conn.cursor()
    conditions = []
    parameters = {}
    if startDate:
        conditions.append('timedate >= :start')
        parameters['start'] = startDate
    if endDate:
        conditions.append('timedate <= :end')
        parameters['end'] = endDate
    if room:
        conditions.append('location = :room')
        parameters['room'] = room

    if conditions:
        conditions = ' WHERE ' + ' AND '.join(conditions)
    else:
        conditions = ''

    q = f'''\
    SELECT
        AVG(temperature) AS temp_avg,
        MAX(temperature) AS temp_max,
        MIN(temperature) AS temp_min,
        AVG((temperature - sub.temp) * (temperature - sub.temp)) AS temp_var,
        AVG(relative_humidity) AS rh_avg,
        MAX(relative_humidity) AS rh_max,
        MIN(relative_humidity) AS rh_min,
        AVG((relative_humidity - sub.rh) * (relative_humidity - sub.rh)) AS rh_var
    FROM measurements,
        (SELECT AVG(temperature) AS temp, AVG(relative_humidity) AS rh FROM measurements {conditions}) AS sub
    {conditions}
    '''

    stats = dict(cur.execute(q, parameters).fetchone())

    if stats['temp_var'] is not None:
        stats['temp_stddev'] = math.sqrt(stats['temp_var'])

    if stats['rh_var'] is not None:
        stats['rh_stddev'] = math.sqrt(stats['rh_var'])

    return stats

def get_rooms():
    conn = get_db()
    cur = conn.cursor()
    rooms = cur.execute('SELECT DISTINCT location FROM measurements')
    return [row['location'] for row in rooms]

def get_measurements(startDate, endDate, room):
    conn = get_db()
    cur = conn.cursor()
    conditions = []
    parameters = {}
    if startDate:
        conditions.append('timedate >= :start')
        parameters['start'] = startDate
    if endDate:
        conditions.append('timedate <= :end')
        parameters['end'] = endDate
    if room:
        conditions.append('location = :room')
        parameters['room'] = room

    if conditions:
        conditions = ' WHERE ' + ' AND '.join(conditions)
    else:
        conditions = ''

    q = f'''\
    SELECT
        timedate, logger_id, location, temperature, relative_humidity
    FROM measurements
    {conditions}
    '''

    return cur.execute(q, parameters)
