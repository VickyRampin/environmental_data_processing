import sqlite3

connection = sqlite3.connect('database.db')

connection.execute("PRAGMA foreign_keys=ON;")

connection.execute("""
    CREATE TABLE uploads (
        upload_id INTEGER PRIMARY KEY AUTOINCREMENT,
        logger_id TEXT NOT NULL,
        location TEXT NOT NULL,
        date_uploaded DATETIME NOT NULL
    );
""")

connection.execute("""
    CREATE TABLE measurements (
        logger_id TEXT NOT NULL,
        location TEXT NOT NULL,
        upload_id INTEGER NOT NULL,
        timedate DATETIME NOT NULL,
        temperature REAL NOT NULL,
        relative_humidity REAL NOT NULL,
        PRIMARY KEY (logger_id, location, timedate),
        FOREIGN KEY (upload_id) REFERENCES uploads(upload_id)
    );
""")
