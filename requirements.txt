bokeh==2.4.3
click==8.1.3
Flask==2.2.2
importlib-metadata==4.12.0
itsdangerous==2.1.2
Jinja2==3.1.2
lxml==4.9.1
MarkupSafe==2.1.1
numpy==1.23.2
packaging==21.3
Pillow==9.2.0
pyparsing==3.0.9
PyYAML==6.0
tornado==6.2
typing-extensions==4.3.0
Werkzeug==2.2.2
zipp==3.8.1
